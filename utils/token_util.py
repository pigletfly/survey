# -- coding: utf8 --
__author__ = 'wangbing'
import sys

reload(sys)
sys.setdefaultencoding('utf-8')
import time
import base64
import hashlib


def create_auth_token(username):
    timestamp = int(time.time())
    secret = '__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__'
    token = '%s%s%s' % (secret, timestamp, username)
    hsh = hashlib.sha1(token).hexdigest()
    return base64.b32encode('%s|%s|%s' % (timestamp, username, hsh))


def verify_token(token, expires=30):
    try:
        token = base64.b32decode(token)
    except:
        return None
    bits = token.split('|')
    if len(bits) != 3:
        return None
    timestamp, username, hsh = bits
    try:
        timestamp = int(timestamp)
        username = str(username)
    except:
        return None
    delta = time.time() - timestamp
    if delta < 0:
        return None
    if delta > expires * 60 * 60 * 24:
        return None
    secret = '__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__'
    _hsh = hashlib.sha1('%s%s%s' % (secret, timestamp, username))
    if hsh == _hsh.hexdigest():
        return True
    return None


if __name__ == '__main__':
    print create_auth_token('bingw@8pang.com')
