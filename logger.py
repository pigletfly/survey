#!/usr/bin/python
# -*- coding: utf-8 -*-
import logging
import os

logging.basicConfig(
    filename=os.path.join("/opt/mon-log/web/trace", 'trace.log'),
    level=logging.DEBUG,
    format='%(asctime)s - %(levelname)s: %(message)s')


def get_log(name="web"):
    return logging.getLogger(name)


if __name__ == "__main__":
    log = get_log()
