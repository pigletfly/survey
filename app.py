# coding=utf-8
import os

import tornado.auth
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.util
from tornado.options import define, options
from tornado.web import url
from mon4web.database import db
import uimodules


define("port", default=8889, help="run on the given port", type=int)
define("debug", default=True, type=bool)


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            url(r'/s/p1', Survey1Handler, name='survey1'),
            url(r'/s/p2', Survey2Handler, name='survey2'),
            url(r'/s/p3', Survey3Handler, name='survey3'),
            url(r'/s/p4', Survey4Handler, name='survey4'),
        ]
        settings = dict(
            debug=options.debug,
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            template_path=os.path.join(os.path.dirname(__file__), 'templates'),
            xsrf_cookies=False,
            cookie_secret="nzjxcjasduuqwheazmu293nsadhaslzkci9023nsadnua9sdads/Vo=",
            ui_modules=uimodules,
            login_url='/',
            sqlalchemy_database_echo=False,
            autoescape=None

        )
        tornado.web.Application.__init__(self, handlers, **settings)


class BaseHandler(tornado.web.RequestHandler):
    def initialize(self):
        self.session = db.session


    @property
    def next_url(self):
        return self.get_argument("next", '')

    def on_finish(self):
        self.session.close()


class Survey1Handler(BaseHandler):
    def get(self):
        # self.render('p1.html')
        self.redirect('http://www.wenjuan.com/s/yQzEfq/', True)
        # if self.get_current_user():
        # self.redirect('/dashboard')
        # else:
        # self.render('index.html', errors=None, mobile='')

        # def post(self):
        # mobile = self.get_argument('mobile')
        # password = pwd2(self.get_argument('password'))
        # # remember = self.get_argument('remember', '')
        # user = User.query.filter(User.mobile == mobile, User.role_type.in_([1, 2])).first()
        # if not user:
        # self.render("index.html", errors=u'手机号不存在!', next=self.next_url, mobile='')
        # elif user.password != password:
        # self.render("index.html", errors='手机号密码不匹配!', next=self.next_url, mobile=mobile)
        #     else:
        #         self.set_secure_cookie('uid', str(user.id))
        #         return self.redirect('/dashboard')

    def post(self):
        pass


class Survey2Handler(BaseHandler):
    def get(self):
        self.render('p2.html')

    def post(self):
        pass


class Survey3Handler(BaseHandler):
    def get(self):
        self.render('p3.html')

    def post(self):
        pass


class Survey4Handler(BaseHandler):
    def get(self):
        self.render('p4.html')

    def post(self):
        pass


def main():
    tornado.options.parse_command_line()
    print 'server started at port %s' % options.port
    http_server = tornado.httpserver.HTTPServer(Application())
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()


if __name__ == '__main__':
    main()
