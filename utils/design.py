#coding=utf-8
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

import xlrd
from xlutils.copy import copy
import requests
from PIL import Image
from StringIO import StringIO
from pyquery import PyQuery as pq
import os
import xlsxwriter
import settings as settings
import utils.date_util as date_util

try:
    import simplejson as json
except ImportError:
    import json

requests.adapters.DEFAULT_RETRIES = 20
user_agent = {
    'User-agent': 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.110 Safari/537.36'}
# proxies = {
#     "http": "http://127.0.0.1:8087",
# }
detail_api = "http://search.cnipr.com/search!doDetailSearch.action"
current_path = os.path.dirname(__file__)
upload_path = settings.UPLOAD_PATH
design_path = settings.DESIGN_PATH
download_path = settings.DOWNLOAD_PATH


def get_design_picture(user_id,filename):
    rb = xlrd.open_workbook(os.path.join(upload_path,filename), formatting_info=True)
    wb = copy(rb)
    rs = rb.sheet_by_index(0)
    ws = wb.get_sheet(0)
    print("%d rows, %d cols" % (rs.nrows, rs.ncols))
    appendix = '.xlsx'
    download_filename = '_'.join((str(user_id),str(date_util.get_cur_ts()))) + appendix
    workbook = xlsxwriter.Workbook(os.path.join(download_path,download_filename))
    format = workbook.add_format()
    format.set_align('center')
    format.set_align('vcenter')
    worksheet = workbook.add_worksheet()
    width = 35
    worksheet.set_column('A:A', width)
    worksheet.set_column('B:B', width)
    worksheet.set_column('C:C', width)
    worksheet.set_column('D:D', width)
    worksheet.set_column('E:E', width)
    worksheet.set_column('F:F', width)
    worksheet.set_column('G:G', width)
    worksheet.set_column('H:H', width)
    worksheet.set_column('I:I', width)
    worksheet.set_column('J:J', width)
    worksheet.set_column('K:K', width)
    worksheet.set_column('L:L', width)
    worksheet.set_column('M:M', width)
    worksheet.set_column('N:N', width)
    for row in range(1, rs.nrows):
        design_num = rs.cell(row, 1).value.strip()
        worksheet.set_row(row, height=160)
        new_dir = os.path.join(design_path, design_num)
        if not os.path.exists(new_dir):
            os.mkdir(new_dir)
        str_where = "申请号,公开（公告）号+=('{design_num}%')".format(design_num=design_num)
        payload = {'strWhere': str_where,
                   'start': 1,
                   'recordCursor': 0,
                   'limit': 1,
                   'option': 2,
                   'iHitPointType': '',
                   'strSortMethod': 'RELEVANCE',
                   'strSources': 'FMZL,SYXX,WGZL',
                   'strSynonymous': '',
                   'yuyijs': '',
                   'filterChannel': ''
        }
        r = requests.post(detail_api, data=payload)
        print r.content
        html = pq(r.content)
        images = html('#spec-list img')
        i = 1
        worksheet.write(row, 0, design_num)
        for image in images:
            image_url = image.get('src')
            print image_url,row
            image_r = requests.get(image_url)
            try:
                image = Image.open(StringIO(image_r.content))
                if image.mode != "RGB":
                    image = image.convert("RGB")
                image.save(os.path.join(design_path+'/'+design_num, str(i) + '.jpg'), 'JPEG', quality=95)
                worksheet.insert_image(row, i, os.path.join(design_path+'/'+design_num, str(i) + '.jpg'),
                                       options=dict(x_offset=30, y_offset=30))
                i += 1
            except Exception as e:
                print e
    workbook.close()
    return download_filename
    # print r.text
    #     validate_day = get_patent_status(status_api.format(patent_app_num=patent_app_num))
    #     if not validate_day:
    #         validate_day = u'无'
    #     print row, validate_day
    #     ws.write(row, 43, validate_day)
    # wb.save('4_result.xls')
    #


# def mkdir(design_num):
#     new_dir = os.path.join(current_path, design_num)
#     if not os.path.exists(new_dir):
#         os.mkdir(new_dir)


if __name__ == '__main__':
    get_design_picture('design.xls')
