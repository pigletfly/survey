# coding=utf-8
from mon4web.database import db

__all__ = ['User']


class User(db.Model):
    __tablename__ = 't_user'
    PER_PAGE = 10

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    role_id = db.Column(db.BigInteger, nullable=False)
    dept_id = db.Column(db.BigInteger)
    check_rule_id = db.Column(db.BigInteger)
    track_rule_id = db.Column(db.BigInteger)
    manager_id = db.Column(db.BigInteger)
    group_id = db.Column(db.BigInteger)
    area_id = db.Column(db.BigInteger)
    is_show = db.Column(db.Integer)
    role_type = db.Column(db.Integer)
    username = db.Column(db.String(32))
    udid = db.Column(db.String(128))
    company_id = db.Column(db.BigInteger)
    mobile = db.Column(db.String(16), nullable=False)
    email = db.Column(db.String(64))
    password = db.Column(db.String(128))
    avatar = db.Column(db.String(164))
    department = db.Column(db.String(64))
    sex = db.Column(db.Integer)
    status = db.Column(db.Integer)
    create_ts = db.Column(db.BigInteger)
    last_login_ts = db.Column(db.BigInteger)
    app_version = db.Column(db.String(16))
    last_visit_client = '无'

    __mapper_args__ = {'order_by': id.desc()}


    def __init__(self, *args, **kwargs):
        super(User, self).__init__(*args, **kwargs)

    def __str__(self):
        return self.username

    def __repr__(self):
        return "<%s>" % self




