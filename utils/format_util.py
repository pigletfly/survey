# coding=utf-8
import mon4web.utils.date_util as dateUtil


def format_date(day):
    return dateUtil.datetime_to_str(dateUtil.str_to_datetime(day), date_format='%m.%d')


def format_float_zero(param):
    return '{0:g}'.format(param)


def group(lst, n):
    """group([0,3,4,10,2,3], 2) => [(0,3), (4,10), (2,3)]

    Group a list into consecutive n-tuples. Incomplete tuples are
    discarded e.g.

    >>> group(range(10), 3)
    [(0, 1, 2), (3, 4, 5), (6, 7, 8)]
    """
    return zip(*[lst[i::n] for i in range(n)])


if __name__ == '__main__':
    print format_float_zero(2.123)
    print '{:.2f}'.format(2.223)
