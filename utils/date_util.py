# coding=utf-8
import datetime
import calendar
import time

WEEK_DAY_NAMES = ["星期一", "星期二", "星期三", "星期四", "星期五", "星期六", "星期日"]

'''  ****************************** 日期类型转换操作工具相关函数************************************'''
#将字符串转换成datetime类型
#返回值为日期类型
def str_to_datetime(datestr, date_format='%Y%m%d'):
    return datetime.datetime.strptime(datestr, date_format)


#时间转换成字符串,格式指定
#返回值为string类型
def datetime_to_str(date, date_format='%Y%m%d'):
    return str(date.strftime(date_format))


def ts_to_str(ts, date_format='%Y-%m-%d %H:%M:%S'):
    if not ts: return ""
    return time.strftime(date_format, time.localtime(ts / 1000))


'''  ****************************** 天操作相关函数************************************'''
#两个日期相隔多少天，例：20081003和20081001是相隔两天
#返回值为间隔天数

def get_date_diff(begin_date, end_date, split=''):
    date_date_format = "%Y" + split + "%m" + split + "%d"
    begin = str_to_datetime(begin_date, date_date_format)
    end = str_to_datetime(end_date, date_date_format)
    return (end - begin).days
    #oneday=datetime.timedelta(days=1)
    #count=0
    #while bd!=ed:
    #    ed=ed-oneday
    #    count+=1
    #return count


#获取两个时间段的所有时间,返回字符串时间段list
def get_day_list(begin_date, end_date, split=''):
    date_format = "%Y" + split + "%m" + split + "%d"
    bd = str_to_datetime(begin_date, date_format)
    ed = str_to_datetime(end_date, date_format)
    oneday = datetime.timedelta(days=1)
    num = get_date_diff(begin_date, end_date, split) + 1
    li = []
    for i in range(0, num):
        li.append(datetime_to_str(ed, date_format))
        ed = ed - oneday
    return li


#获取给定参数的前num天的日期，返回一个日期list
def get_prev_day_list(day, num, split=''):
    date_format = "%Y" + split + "%m" + split + "%d"
    today = str_to_datetime(day, date_format)
    oneday = datetime.timedelta(days=1)
    li = []
    for i in range(0, num):
        #今天减一天，一天一天减
        today = today - oneday
        #把日期转换成字符串
        #result=datetostr(today)
        li.append(datetime_to_str(today, date_format))
    return li


#获取给定参数的后num天的日期，返回一个日期list
def get_next_day_list(day, num, split=''):
    date_format = "%Y" + split + "%m" + split + "%d"
    today = str_to_datetime(day, date_format)
    oneday = datetime.timedelta(days=1)
    li = []
    for i in range(0, num):
        #今天加一天，一天一天加
        today = today + oneday
        #把日期转换成字符串
        #result=datetostr(today)
        li.append(datetime_to_str(today, date_format))
    return li


def get_prev_day(day, num, split=''):
    date_format = "%Y" + split + "%m" + split + "%d"
    today = str_to_datetime(day, date_format)
    oneday = datetime.timedelta(days=num)
    return datetime_to_str(today - oneday, date_format)


def get_delta_day(date, num):
    d = str_to_datetime(date)
    return datetime_to_str(d + datetime.timedelta(days=num))


#获取今日的日期
def get_cur_ts():
    return int(time.time() * 1000)


def get_cur_hour():
    return time.localtime().tm_hour


def get_today(split=''):
    date_format = "%Y" + split + "%m" + split + "%d"
    return datetime.date.today().strftime(date_format)


#获取昨日的日期
def get_yesterday(split=''):
    date_format = "%Y" + split + "%m" + split + "%d"
    today = datetime.date.today()
    oneday = datetime.timedelta(days=1)
    return (today - oneday).strftime(date_format)


#获取明日的日期
def get_tomorrow(split=''):
    date_format = "%Y" + split + "%m" + split + "%d"
    t = get_today()
    return (t + datetime.timedelta(days=1)).strftime(date_format)


#获取输入日期上一日的日期
def get_last_day(day, split=''):
    date_format = "%Y" + split + "%m" + split + "%d"
    thisday = str_to_datetime(day, date_format)
    oneday = datetime.timedelta(days=1)
    return (thisday - oneday).strftime(date_format)


#获取输入日期下一日的日期
def get_next_day(day, split=''):
    date_format = "%Y" + split + "%m" + split + "%d"
    thisday = str_to_datetime(day, date_format)
    oneday = datetime.timedelta(days=1)
    return (thisday + oneday).strftime(date_format)


'''  ****************************** 周操作相关函数************************************'''


def get_monday(date, split=''):
    week_day = get_week_day(date, split)
    if week_day == 1:
        return date
    else:
        return get_delta_day(date, -(week_day - 1))


def get_last_week(date, split=''):
    date_format = "%Y" + split + "%m" + split + "%d"
    thisday = str_to_datetime(date, date_format)
    d = datetime.timedelta(weeks=1)  # 20周将被自动转化为天数[luther.gliethttp]
    return (thisday - d).strftime(date_format)


def get_next_week(date, split=''):
    date_format = "%Y" + split + "%m" + split + "%d"
    thisday = str_to_datetime(date, date_format)
    d = datetime.timedelta(weeks=1)  # 20周将被自动转化为天数[luther.gliethttp]
    return (thisday + d).strftime(date_format)


def get_week_date_list(year, stat_week):
    year_first_day = datetime.date(year, 0o1, 1)
    stat_week_days = datetime.timedelta(weeks=stat_week - 1)
    stat_week_day = year_first_day + stat_week_days
    stat_week_day_str = datetime_to_str(stat_week_day)
    return [get_next_day(stat_week_day_str), get_next_week(stat_week_day_str)]


#获取指定日期的是当年的第几天和第几周
def get_week_day(date, split=''):
    date_format = "%Y" + split + "%m" + split + "%d"
    thisday = str_to_datetime(date, date_format)
    return thisday.weekday() + 1


def get_week_day_name(date, split=''):
    week_day = get_week_day(date, split)
    if week_day < 1:
        week_day = 1
    elif week_day > 7:
        week_day = 7
    return WEEK_DAY_NAMES[week_day - 1]


def get_year_week(date, split=''):
    date_format = "%Y" + split + "%m" + split + "%d"
    thisday = str_to_datetime(date, date_format)
    return thisday.isocalendar()


def get_week_num(date, split=''):
    '''获得给定日期是这一年的第几周。
    每周以周一为一周的开始，但1月1日不是周一时,算作上一年的最后一周,返回0'''
    date_format = "%Y" + split + "%m" + split + "%d"
    date = str_to_datetime(date, date_format)
    if not date:
        t = get_today()
        date = datetime.datetime(t.year, t.month, t.day)
    year = date.year
    wd = calendar.weekday(year, 1, 1)
    days = (date - datetime.datetime(year, 1, 1)).days
    nweek = 0
    if wd:
        nweek = (days + wd) / 7
    else:
        nweek = days / 7 + 1
    return nweek


def get_week_info(date, split=''):
    year = date[:4]
    nweek = get_week_num(date)
    #修改年份
    if nweek == 0:
        last_year = str(int(year) - 1)
        return last_year + str(get_week_num(last_year + "1231"))
    if nweek < 10: nweek = "0" + str(nweek)  #补零
    return year + str(nweek)


def get_last_week_day_list(date, split=''):
    year_week = get_year_week(date, split)
    return get_prev_day_list(get_prev_day(date, year_week[2] - 1, split), 7, split)


def get_this_week_day_list(date, split=''):
    year_week = get_year_week(date)
    return get_next_day_list(get_prev_day(date, year_week[2], split), 7, split)


'''  ****************************** 月操作相关函数************************************'''
#获取当前日期月份 字符串
def get_current_month():
    return str(datetime.date.today())[5:7]


#获取输入日期的月份 字符串
def get_month(date, split=''):
    date_format = "%Y" + split + "%m" + split + "%d"
    thisday = str_to_datetime(date, date_format)
    return str(thisday)[5:7]


#获取当前日期的上月
def get_last_month(date):
    if date:
        curMonth = datetime.datetime(int(date[0:4]), int(date[4:6]), 1)
    else:
        t = get_today()
        curMonth = datetime.datetime(t.year, t.month, 1)
    lastMonth = curMonth + datetime.timedelta(days=-1)
    return str(lastMonth.year) + "%02d" % lastMonth.month


#获取输入日期的上个月第一天日期
def get_last_month_first_day(date, split=''):
    date_format = "%Y" + split + "%m" + split + "%d"
    thisday = str_to_datetime(date, date_format)
    #上一个月的第一天
    lastmonth = datetime_to_str(datetime.date(thisday.year, thisday.month, 1) - datetime.timedelta(days=1), date_format)
    last_month_first_day = str_to_datetime(get_this_month_first_day(lastmonth, split), date_format)
    return last_month_first_day.strftime(date_format)


#获取输入日期的上个月最后一天日期
def get_last_month_last_day(date, split=''):
    date_format = "%Y" + split + "%m" + split + "%d"
    thisday = str_to_datetime(date, date_format)
    #上一个月的最后一天
    last_month_last_day = datetime.date(thisday.year, thisday.month, 1) - datetime.timedelta(1)
    return last_month_last_day.strftime(date_format)


#获取输入日期的当月第一天日期
def get_this_month_first_day(date, split=''):
    date_format = "%Y" + split + "%m" + split + "%d"
    thisday = str_to_datetime(date, date_format)
    lst_last = datetime.date(thisday.year, thisday.month, 1)
    return lst_last.strftime(date_format)


#获取输入日期的当月最后一天日期
def get_this_month_last_day(date, split=''):
    if get_month(date, split) == '12':
        return get_year(date, split) + "1231"
    date_format = "%Y" + split + "%m" + split + "%d"
    thisday = str_to_datetime(date, date_format)
    #上一个月的最后一天
    lst_last = datetime.date(thisday.year, thisday.month + 1, 1) - datetime.timedelta(1)
    return lst_last.strftime(date_format)


'''  ****************************** 年操作相关函数************************************'''
#获取当前年份 是一个字符串
def get_current_year():
    return str(datetime.date.today())[0:4]


#获取指定日期的年
def get_year(date, split=''):
    date_format = "%Y" + split + "%m" + split + "%d"
    thisday = str_to_datetime(date, date_format)
    return str(thisday)[0:4]


def get_age(birthday, today=''):
    '''
    周岁年龄
    '''
    if not today: today = get_today()
    days = get_date_diff(birthday, today)
    age = days / 365
    age = 365 * age <= (days - age / 4) and age or age - 1
    if age < 0: age = 0
    return age


def get_current_slice():
    now = datetime.datetime.now()
    hour = now.hour
    record_slice = 1
    if 0 <= hour < 11:
        record_slice = 1
    elif 11 <= hour < 16:
        record_slice = 2
    elif 16 <= hour < 21:
        record_slice = 3
    else:
        record_slice = 4
    return record_slice


def get_chinese_display(ts, format="%Y年%m月%d日 %H:%M"):
    date_str = ts_to_str(ts, format)
    return date_str


if __name__ == "__main__":
    #print(get_prev_day('20120101',1))
    #print(get_week_day("20130407"))
    #print(get_date_diff("19840607","20130607"))
    #print(get_age('19840607','20150606'))
    #print(get_chinese_display(get_cur_ts()))
    #print(get_yesterday())
    #print(get_today())
    #print(get_week_info("20130326"))
    print(ts_to_str(1400768996819, "%Y%m%d%H%M"))
    #print get_last_week_day_list(get_today())
    #start = get_this_week_day_list(get_today())[0]
    #end = get_this_week_day_list(get_today())[6]
    #print start >= end
    #print get_next_week(get_today())
    #print get_last_week(get_today())
    #print get_today()
    #print get_cur_hour()
    #print(get_age('20120101'))
    #print(get_cur_hour())
    #print(get_monday('20130401'))
    #print(get_delta_day('20130401',-7))
    #print(get_week_day_name('20130401'))
    print(ts_to_str(1401071996356))
    #print(get_week_num('20130422'))
