$(document).ready(function () {
    $('.dateselect').datepicker({
        format: "yyyymmdd",
        language: "zh-CN",
        autoclose: true,
        todayHighlight: true
    });
    $('.fancybox').fancybox(
        {
            'transitionIn': 'elastic',
            'transitionOut': 'elastic',
            'titlePosition': 'inside'
        }
    );
    $('.multiselect').multiselect();

    $(".area").change(function () {
        $.ajax({
            type: 'GET',
            cache: false,
            url: '/ajax/dept?area=' + $(this).val(),
            success: function (data) {
                $('.dept').empty();
                $.each(data, function (i, val) {
                    $('.dept').append('<option value="' + val['id'] + '">' + val['department_name'] + '</option>')
                });
            },
            dataType: 'json'
        });
    });
    $('input, textarea').placeholder();
    $("#tab-content a").click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });
//    var url = '/upload';
//    $('#fileupload').fileupload({
//        url: url,
//        dataType: 'json',
//        done: function (e, data) {
//            console.log(data);
//            console.log(data.result.download_file);
////            $('#download').attr('href', '/static/download/' + data.result.download_file);
////            $('#download').css('display','inline-block');
////            $('.alert-success').show();
//        },
//        progressall: function (e, data) {
//            var progress = parseInt(data.loaded / data.total * 100, 10);
//            $('#progress .progress-bar').css(
//                'width', progress + '%'
//            );
//        }
//    }).prop('disabled', !$.support.fileInput)
//        .parent().addClass($.support.fileInput ? undefined : 'disabled');
//    $(document).delegate('*[data-toggle="lightbox"]', 'click', function (event) {
//        event.preventDefault();
//        $(this).ekkoLightbox();
//    });

//    $.datepicker.regional['zh-CN'] = {
//        closeText: '关闭',
//        prevText: '<上月',
//        nextText: '下月>',
//        currentText: '今天',
//        monthNames: ['一月', '二月', '三月', '四月', '五月', '六月',
//            '七月', '八月', '九月', '十月', '十一月', '十二月'
//        ],
//        monthNamesShort: ['1', '2', '3', '4', '5', '6',
//            '7', '8', '9', '10', '11', '12'
//        ],
//        dayNames: ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],
//        dayNamesShort: ['周日', '周一', '周二', '周三', '周四', '周五', '周六'],
//        dayNamesMin: ['日', '一', '二', '三', '四', '五', '六'],
//        weekHeader: '周',
//        dateFormat: 'yymmdd',
//        defaultDate: new Date(1985, 00, 01),
//        yearRange: '1960:2020',
//        firstDay: 1,
//        isRTL: false,
//        showMonthAfterYear: true
//        //        monthSuffix: '月',
//        //        yearSuffix: '年'
//    };
//    $.datepicker.setDefaults($.datepicker.regional['zh-CN']);
//    //日期选择框格式和语言
//    $('#begin-date').datepicker({
//        changeMonth: true,
//        changeYear: true
//    });
//    $('#end-date').datepicker({
//        changeMonth: true,
//        changeYear: true
//    });
//    $('#birthday-datepicker').datepicker({
//        changeMonth: true,
//        changeYear: true
//    });
//    $('#update-birthday-datepicker').datepicker({
//        changeMonth: true,
//        changeYear: true
//    });
    //搜索
    $('#trackmap-search').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            username: {
//                trigger: 'blur',
                validators: {
                    notEmpty: {
                        message: '人员不能为空'
                    }
                }
            }
        }
    });
    //登录表单
    $('#signin-form').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            mobile: {
                trigger: 'blur',
                validators: {
                    notEmpty: {
                        message: '手机号码不能为空'
                    },
                    mobile: {
                        message: '手机号码格式不正确'
                    }
                }
            },
//            email: {
//                trigger: 'blur',
//                validators: {
//                    notEmpty: {
//                        message: '邮箱不能为空'
//                    },
//                    emailAddress: {
//                        message: '邮箱格式不正确'
//                    }
//                }
//            },
            password: {
                validators: {
                    notEmpty: {
                        message: '密码不能为空'
                    }
                }
            }
        }
    });
    //注册表单
    $('#register-form').bootstrapValidator({
        message: 'This value is not valid',
        fields: {
//            username: {
//                validators: {
//                    notEmpty: {
//                        message: '用户名不能为空'
//                    }
//                }
//            },
            email: {
                trigger: 'blur',
                validators: {
                    notEmpty: {
                        message: '邮箱不能为空'
                    },
                    emailAddress: {
                        message: '邮箱格式不正确'
                    }
                }
            },
            mobile: {
                trigger: 'blur',
                validators: {
                    notEmpty: {
                        message: '手机号码不能为空'
                    },
                    mobile: {
                        message: '手机号码格式不正确'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: '密码不能为空'
                    }
                }
            },
            invite_code: {
                validators: {
                    notEmpty: {
                        message: '激活码不能为空'
                    }
                }
            }
        }
    });
    //找回密码表单
    $('#forgot-form').bootstrapValidator({
        message: 'This value is not valid',
        fields: {
//            username: {
//                validators: {
//                    notEmpty: {
//                        message: '用户名不能为空'
//                    }
//                }
//            },
            mobile: {
                trigger: 'blur',
                validators: {
                    notEmpty: {
                        message: '手机号码不能为空'
                    },
                    mobile: {
                        message: '手机号码格式不正确'
                    }
                }
            },
            email: {
                trigger: 'blur',
                validators: {
                    notEmpty: {
                        message: '邮箱不能为空'
                    },
                    emailAddress: {
                        message: '邮箱格式不正确'
                    }
                }
            }
        }
    });
    //修改密码model
    $('#passModel').bootstrapValidator({
        message: 'This value is not valid',
        fields: {
            password: {
                validators: {
                    notEmpty: {
                        message: '原密码不能为空'
                    },
                    remote: {
                        url: '/check_password',
                        message: '原密码不正确'
                    }
                }
            },
            new_password: {
                validators: {
                    notEmpty: {
                        message: '新密码不能为空'
                    },
                    identical: {
                        field: 'repeat_password',
                        message: '新密码和重复密码不一致'
                    }
                }
            },
            repeat_password: {
                validators: {
                    notEmpty: {
                        message: '重复密码不能为空'
                    },
                    identical: {
                        field: 'new_password',
                        message: '新密码和重复密码不一致'
                    }
                }
            }
        }
    });
    //创建model
    $('#groupModel').bootstrapValidator({
        message: 'This value is not valid',
        fields: {
            clazz_name: {
                validators: {
                    notEmpty: {
                        message: '班级名称不能为空'
                    },
                    remote: {
                        url: '/check_clazz',
                        message: '班级名称已存在'
                    }
                }
            }
        }
    });
    //编辑小组model
    $('#groupEditModel').bootstrapValidator({
        message: 'This value is not valid',
        fields: {
            clazz_name: {
                validators: {
                    notEmpty: {
                        message: '班级名称不能为空'
                    },
                    remote: {
                        url: '/check_clazz?edit=1',
                        message: '班级名称已存在'
                    }
                }
            }
        }
    });
    //邀请model
    $('#inviteModal').bootstrapValidator({
        message: 'This value is not valid',
        submitHandler: function (validator, form) {
            // validator is the BootstrapValidator instance
            // form is the jQuery object present the current form
            $.ajax({
                type: "POST",
                url: '/invite',
                data: $("#inviteModal").serialize(),
                dataType: 'json',
                success: function (data) {
                    window.location.reload();
//                    $('#inviteModal').removeData('modal');
//                    $('#inviteModal').modal('hide');
//                    return true;
                },
                error: function (data) {
                }
            });
//            form.submit();
        },
        fields: {
            realname: {
                validators: {
                    notEmpty: {
                        message: '姓名不能为空'
                    }
                }
            },
            init_weight_num: {
                validators: {
                    notEmpty: {
                        message: '体重不能为空'
                    },
                    number: {
                        message: '体重必须为数字'
                    }
                }
            },
            height: {
                validators: {
                    notEmpty: {
                        message: '身高不能为空'
                    },
                    number: {
                        message: '身高必须为数字'
                    }
                }
            },
            birthday: {
                validators: {
                    notEmpty: {
                        message: '生日不能为空'
                    }
                }
            },
            mobile: {
                trigger: 'blur',
                validators: {
                    notEmpty: {
                        message: '手机号码不能为空'
                    },
                    mobile: {
                        message: '手机号码格式不正确'
                    },
                    remote: {
                        url: '/check_mobile',
                        message: '手机号码已经被使用'
                    }
                }
            },
            email: {
                trigger: 'blur',
                validators: {
//                    notEmpty: {
//                        message: '邮箱不能为空'
//                    },
                    emailAddress: {
                        message: '邮箱格式不正确'
                    }
//                    remote: {
//                        url: '/check_email',
//                        message: '邮箱已存在'
//                    }
                }
            }
        }
    });
    $('input[type=file]').change(function (e) {
        $('#file_name').html($(this).val());
    });
    //更新个人资料
    $('#updateProfileModal').bootstrapValidator({
        message: 'This value is not valid',
//        submitHandler: function (validator, form) {
//            $.ajax({
//                type: "POST",
//                url: '/profile',
//                data: $("#updateProfileModal").serialize(),
//                dataType: 'json',
//                success: function (data) {
//                    window.location.reload();
////                    $('#inviteModal').removeData('modal');
////                    $('#inviteModal').modal('hide');
////                    return true;
//                },
//                error: function (data) {
//                }
//            });
////            form.submit();
//        },
        fields: {
            realname: {
                trigger: 'blur',
                validators: {
                    notEmpty: {
                        message: '姓名不能为空'
                    }
                }
            },
            init_weight_num: {
                trigger: 'blur',
                validators: {
                    notEmpty: {
                        message: '体重不能为空'
                    },
                    number: {
                        message: '体重必须为数字'
                    }
                }
            },
            height: {
                trigger: 'blur',
                validators: {
                    notEmpty: {
                        message: '身高不能为空'
                    },
                    number: {
                        message: '身高必须为数字'
                    }
                }
            },
            birthday: {
                trigger: 'blur',
                validators: {
                    notEmpty: {
                        message: '生日不能为空'
                    }
                }
            },
            mobile: {
                trigger: 'blur',
                validators: {
                    notEmpty: {
                        message: '手机号码不能为空'
                    },
                    mobile: {
                        message: '手机号码格式不正确'
                    },
                    remote: {
                        url: '/check_mobile?edit=1',
                        message: '手机号码已经被使用'
                    }
                }
            },
            email: {
                trigger: 'blur',
                validators: {
//                    notEmpty: {
//                        message: '邮箱不能为空'
//                    },
                    emailAddress: {
                        message: '邮箱格式不正确'
                    }
//                    remote: {
//                        url: '/check_email',
//                        message: '邮箱已存在'
//                    }
                }
            }
        }
    });

});


$.ajaxSetup({
    beforeSend: function (jqXHR, settings) {
        type = settings.type
        if (type != 'GET' && type != 'HEAD' && type != 'OPTIONS') {
            var pattern = /(.+; *)?_xsrf *= *([^;" ]+)/;
            var xsrf = pattern.exec(document.cookie);
            if (xsrf) {
                jqXHR.setRequestHeader('X-Xsrftoken', xsrf[2]);
            }
        }
    }});
function delete_group(group_id) {
    if (confirm("确定要删除班级吗？")) {
        $.ajax({
            type: 'DELETE',
            cache: false,
            url: '/groups?group_id=' + group_id,
            success: function (data) {
                if (data == true) {
//                    $(element).parent().parent().parent().next('hr').remove();
//                    $(element).parent().parent().parent().remove();
                    window.location.reload();
                }
            },
            dataType: 'json'
        });
    }
}
function start_craw() {
    $.ajax({
        type: 'GET',
        cache: false,
        url: '/craw',
        success: function (data) {
            console.log(data);
            $('#download').attr('href', '/static/download/' + data.download_file);
            $('#download').css('display', 'inline-block');
            $('.alert-success').show();
        },
        dataType: 'json'
    });
}
function edit_group(clazz_id, clazz_name) {
    $('#edit_group_id').val(clazz_id);
    $('#edit_group_name').val(clazz_name);
    $('#groupEditModel').modal({ show: true });
}
//修改个人资料
function edit_user_profile(user_id, realname, mobile, sex, weight_num, height, birthday, group_id, email, caloric_type) {
    $('#update_user_id').val(user_id);
    $('#update_realname').val(realname);
    $('#update_mobile').val(mobile);
    $('#update_sex').val(sex);
    $('#update_caloric_type').val(caloric_type);
    $('#update_weight_num').val(weight_num);
    $('#update_height').val(height);
    $('#update-birthday-datepicker').val(birthday);
    $('#update_group_id').val(group_id);
    $('#update_email').val(email);
    $('#updateProfileModal').modal({ show: true });
}
function check_avatar() {
    avatar = $('#file_name').html();
    if (avatar == '') {
        alert('请选择头像图片!');
        return false;
    } else
        return true;
}


var mapObj;
//初始化地图对象，加载地图
function mapInit() {
    mapObj = new AMap.Map("iCenter", {
        //二维地图显示视口
        view: new AMap.View2D({
            center: new AMap.LngLat(116.397428, 39.90923),//地图中心点
            zoom: 17 //地图显示的缩放级别
        }),
        continuousZoomEnable: false
    });
    AMap.event.addListener(mapObj, "complete", completeEventHandler);
}

//地图图块加载完毕后执行函数
function completeEventHandler() {
    marker = new AMap.Marker({
        map: mapObj,
        //draggable:true, //是否可拖动
        position: new AMap.LngLat(116.397428, 39.90923),//基点位置
        icon: "http://code.mapabc.com/images/car_03.png", //marker图标，直接传递地址url
        offset: new AMap.Pixel(-26, -13), //相对于基点的位置
        autoRotation: true
    });

    var lngX = 116.397428;
    var latY = 39.90923;
    lineArr = [];
    lineArr.push(new AMap.LngLat(lngX, latY));
    for (var i = 1; i < 3; i++) {
        lngX = lngX + Math.random() * 0.05;
        if (i % 2) {
            latY = latY + Math.random() * 0.0001;
        } else {
            latY = latY + Math.random() * 0.06;
        }
        lineArr.push(new AMap.LngLat(lngX, latY));
    }
    //绘制轨迹
    var polyline = new AMap.Polyline({
        map: mapObj,
        path: lineArr,
        strokeColor: "#00A",//线颜色
        strokeOpacity: 1,//线透明度
        strokeWeight: 3,//线宽
        strokeStyle: "solid"//线样式
    });
    mapObj.setFitView();
}

function startAnimation() {
    console.log(marker);
    marker.moveAlong(lineArr, 500);
}

function test() {
    console.log(marker.getPosition());
    mapObj.panTo(marker.getPosition());
}
function stopAnimation() {
    marker.stopMove();
}
function add_role() {
    $('#role-list').append('<div class="col-md-4" style="padding-top: 10px"><input type="text" class="form-control" name="role-name"></div>');

}
function show_user_modal(user_id) {
    $('#userModal').removeData('bs.modal');
    $('#userModal').modal({
        show: true,
        remote: '/user/modal?uid=' + user_id
    });
}
function delete_user(user_id) {
    if (confirm("确定要删除用户？")) {
        $.ajax({
            type: 'DELETE',
            cache: false,
            url: '/user/modal?uid=' + user_id,
            success: function (data) {
                console.log(data);
//                if (data == true) {
                window.location.reload();
//                }
            },
            dataType: 'json'
        });
    }
}
function show_group_menu_modal(user_id) {
    $('#groupMenuModal').removeData('bs.modal');
    $('#groupMenuModal').modal({
        show: true,
        remote: '/group/menu/modal?company_group_id=' + user_id
    });
}
function show_company_role_modal(company_role_id) {
    $('#companyRoleModal').removeData('bs.modal');
    $('#companyRoleModal').modal({
        show: true,
        remote: '/company/role/modal?company_role_id=' + company_role_id
    });
}
function show_check_rule_modal(check_rule_id) {
    $('#checkRuleModal').removeData('bs.modal');
    $('#checkRuleModal').modal({
        show: true,
        remote: '/check/rule/modal?check_rule_id=' + check_rule_id
    });
}
function show_track_rule_modal(track_rule_id) {
    $('#trackRuleModal').removeData('bs.modal');
    $('#trackRuleModal').modal({
        show: true,
        remote: '/track/rule/modal?track_rule_id=' + track_rule_id
    });
}
function delete_group_menu(group_id) {
    if (confirm("确定要删除群组？")) {
        $.ajax({
            type: 'DELETE',
            cache: false,
            url: '/group/menu/modal?gid=' + group_id,
            success: function (data) {
                console.log(data);
//                if (data == true) {
                window.location.reload();
//                }
            },
            dataType: 'json'
        });
    }
}
function delete_company_role(company_role_id) {
    if (confirm("确定要删除角色？")) {
        $.ajax({
            type: 'DELETE',
            cache: false,
            url: '/company/role/modal?rid=' + company_role_id,
            success: function (data) {
                console.log(data);
//                if (data == true) {
                window.location.reload();
//                }
            },
            dataType: 'json'
        });
    }
}
function delete_check_rule(check_rule_id) {
    if (confirm("确定要删除考勤规则？")) {
        $.ajax({
            type: 'DELETE',
            cache: false,
            url: '/check/rule/modal?cid=' + check_rule_id,
            success: function (data) {
                console.log(data);
//                if (data == true) {
                window.location.reload();
//                }
            },
            dataType: 'json'
        });
    }
}
function delete_track_rule(track_rule_id) {
    if (confirm("确定要删除定位规则？")) {
        $.ajax({
            type: 'DELETE',
            cache: false,
            url: '/track/rule/modal?tid=' + track_rule_id,
            success: function (data) {
                window.location.reload();
            },
            dataType: 'json'
        });
    }
}
function unlink_mobile(user_id) {
    if (confirm("确定要解绑手机？")) {
        $.ajax({
            type: 'PUT',
            cache: false,
            url: '/user/modal?uid=' + user_id,
            success: function (data) {
                console.log(data);
//                if (data == true) {
                window.location.reload();
//                }
            },
            dataType: 'json'
        });
    }
}
function reset(user_id) {
    if (confirm("确定要重置密码？")) {
        $.ajax({
            type: 'POST',
            cache: false,
            url: '/reset?uid=' + user_id,
            success: function (data) {
                console.log(data);
//                if (data == true) {
                window.location.reload();
//                }
            },
            dataType: 'json'
        });
    }
}
function show_company_modal(company_id) {
    $('#companyModal').removeData('bs.modal');
    $('#companyModal').modal({
        show: true,
        remote: '/company/modal?cid=' + company_id
    });
}
function delete_company(company_id) {
    if (confirm("确定要删除公司？")) {
        $.ajax({
            type: 'DELETE',
            cache: false,
            url: '/company/modal?cid=' + company_id,
            success: function (data) {
                console.log(data);
//                if (data == true) {
                window.location.reload();
//                }
            },
            dataType: 'json'
        });
    }
}