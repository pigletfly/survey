# coding=utf-8

import types, random, re
import date_util as dateUtil


def sex_type(sexType):
    sexType = int(sexType)
    return sexType == 1 and '男' or '女'


def format_float(f, fixed=2):
    if fixed == 2:
        return '%.2f' % f
    elif fixed == 1:
        return '%.1f' % f


def format_float_fixed(f):
    f = float(format_float(f))
    return "{0:g}".format(f)


def format_hour(num):
    if int(num) < 10:
        return "0" + str(num)
    else:
        return str(num)


def null2empty(obj):
    if type(obj) == types.NoneType:
        return ""
    elif type(obj) == types.IntType:
        return str(obj)
    else:
        return obj


def null2zero(v):
    if v == None or v == '':
        return 0
    return v


def zero2empty(v):
    if v == None or v == "" or int(v) == 0:
        return ''
    return v


def concat_infile_str(*args):
    concat_str = str(args[0])
    for arg in args[1:]:
        if not arg: arg = "0"  #空值
        if isinstance(arg, unicode): arg = arg.encode("utf-8")  #unicode编码
        concat_str += "," + str(arg)
    return concat_str + "\n"


def volidate_str_len(str, slen):
    if len(str) > slen:
        return str[:slen]
    else:
        return str


def volidate_int(str):
    if type(str) == types.IntType:
        return int(str)
    else:
        return 0


def course_status(beginDay, endDay):
    today = int(dateUtil.get_today())
    if endDay and int(endDay) < today:
        return "已结束"
    elif beginDay and int(beginDay) <= today:
        return "<font class='red'>进行中</font>"
    else:
        return "未开始"


def build_random(num):
    r = ""
    for i in xrange(0, num):
        r += str(random.randint(0, 9))
    return r


def build_ascii_special(num):
    r = ""
    for i in xrange(0, num):
        r += chr(num)
    #print(len(r))
    return r


def radio_percent(a, b):
    radio = float(a) * 100 / float(b)
    if radio > 100: return 100
    return format_float(radio)


def unicode2str(u):
    if u and types.UnicodeType == type(u):
        return u.encode("utf-8")
    else:
        return str(u)


def str2unicode(u):
    if not isinstance(u, unicode):
        u = u.decode("utf-8")
    return u


def is_number(s):
    if s == None: return False
    if re.match('^(-?\d+)(\.\d+)?$', s):
        return True
    else:
        return False


def is_hex(num):
    nums = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 'a', 'b', 'c', 'd', 'e', 'f']
    return num in nums


def is_han(s):
    if not isinstance(s, unicode): s = s.decode("utf-8")
    hanRegx = re.compile(ur"([\u4E00-\u9FA5]+)")
    return hanRegx.match(s)


def has_han(s):
    if not isinstance(s, unicode): s = s.decode("utf-8")
    chars = re.findall(ur"[\u4E00-\u9FA5]", s)
    return chars


def dec2hex(num):
    return hex(num)


def hex2dec(num):
    return int(str(num), 16)


def round_num(num):
    if not num: return 0
    return int(round(float(num)))


if __name__ == "__main__":
    #print(format_float(3.40))
    print(format_float_fixed(3.0))
    #print(build_random(6))
    #print(radio_percent('21',65))
    #print(unicode2str(u"asdf"))
    print(is_number(""))
    #print(float(0123))
    #print(dec2hex(18))
    #print(hex2dec(8))
    #print(build_ascii_special(10))
    print(is_han("711中国"))
    print(has_han("711中国"))
    #print(round_num("389.45"))
