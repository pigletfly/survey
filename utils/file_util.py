#coding=utf-8
'''
file util
'''

import os
import date_util as dateUtil
import string_util as stringUtil

def upload_img(sourceImg,savePath,needNewName=True):
    """
    file upload util for tornado
    """
    if sourceImg.filename:
        #build filename
        fileName=sourceImg.filename.replace('\\','/')
        fileName=fileName.split("/")[-1]
        if needNewName and fileName:
            fileExt=fileName.split(".")[-1]
            fileName=str(dateUtil.get_cur_ts())+stringUtil.build_random(3)+"."+fileExt
        #write
        if fileName:
            savePath=os.path.join(savePath,fileName)
            print(savePath)
            with open(savePath,"wb") as fw:
                fw.write(sourceImg.body)
        return fileName 

def delete_file(filepath):
    if os.path.isfile(filepath):
        os.remove(filepath)


if __name__=="__main__":
    delete_file("/Users/xiaoma_itnet/Workspace/qingbii/qingbii-server/qingbii4teacher/web/static/upload_img/class/1368516897229813.jpg")

        

