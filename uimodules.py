import tornado.web
import logging
from mon4web.models.user import User
from mon4web.models.role import Role
from mon4web.models.area import Area
from mon4web.models.dept import Dept
from mon4web.models.group_menu import GroupMenu
import urlparse


class Form(tornado.web.UIModule):
    """
    Generic form rendering module. Works with wtforms.
    Use this in your template code as:

    {% module Form(form) %}

    where `form` is a wtforms.Form object. Note that this module does not render
    <form> tag and any buttons.
    """

    def render(self, form):
        """docstring for render"""
        return self.render_string('uimodules/form.html', form=form)


class Header(tornado.web.UIModule):
    def render(self):
        """docstring for render"""
        user = User.query.get(self.current_user)
        return self.render_string('uimodules/header.html', user=user)


class Menu(tornado.web.UIModule):
    def render(self):
        """docstring for render"""
        user = User.query.get(self.current_user)
        group_id = user.group_id
        menus = GroupMenu.query.filter(GroupMenu.group_id == group_id).all()
        access_menus = []
        for menu in menus:
            access_menus.append(menu.menu_id)
        print access_menus
        return self.render_string('uimodules/menu.html', uri=self.request.uri, user=user, access_menus=access_menus)


class Page(tornado.web.UIModule):
    def render(self, page):
        """docstring for render"""
        uri = self.request.uri.split('?')[0]
        result = urlparse.urlparse(self.request.uri)
        params = urlparse.parse_qs(result.query, True)
        other_params = ''
        params.pop('page', None)
        for k, v in params.iteritems():
            other_params += '&' + str(k) + '=' + str(v[0])
        return self.render_string('uimodules/page.html', uri=uri, page=page, other_params=other_params)


class SimplePage(tornado.web.UIModule):
    def render(self, page):
        """docstring for render"""
        uri = self.request.uri.split('?')[0]
        result = urlparse.urlparse(self.request.uri)
        params = urlparse.parse_qs(result.query, True)
        other_params = ''
        params.pop('page', None)
        for k, v in params.iteritems():
            other_params += '&' + str(k) + '=' + str(v[0])
        return self.render_string('uimodules/simple-page.html', uri=uri, page=page, other_params=other_params)


class RoleSelect(tornado.web.UIModule):
    def render(self, role_id=None):
        """docstring for render"""
        roles = Role.query.filter(Role.id >= 2).all()
        return self.render_string('uimodules/role_select.html', roles=roles, role_id=role_id)


class AreaSelect(tornado.web.UIModule):
    def render(self, area_id=None, company_id=None):
        """docstring for render"""
        areas = []
        if company_id:
            areas = Area.query.filter(Area.company_id == company_id).all()
        return self.render_string('uimodules/area_select.html', areas=areas, area_id=area_id, company_id=company_id)


class AreaQuerySelect(tornado.web.UIModule):
    def render(self, area_id=None, company_id=None):
        """docstring for render"""
        areas = []
        if company_id:
            areas = Area.query.filter(Area.company_id == company_id).all()
        return self.render_string('uimodules/area_query_select.html', areas=areas, area_id=area_id,
                                  company_id=company_id)


class DeptSelect(tornado.web.UIModule):
    def render(self, dept_id=None, company_id=None):
        """docstring for render"""
        depts = []
        if company_id:
            depts = Dept.query.filter(Dept.company_id == company_id).all()
        return self.render_string('uimodules/dept_select.html', depts=depts, dept_id=dept_id, company_id=company_id)


class DeptQuerySelect(tornado.web.UIModule):
    def render(self, dept_id=None, company_id=None):
        """docstring for render"""
        depts = []
        if company_id:
            depts = Dept.query.filter(Dept.company_id == company_id).all()
        return self.render_string('uimodules/dept_query_select.html', depts=depts, dept_id=dept_id,
                                  company_id=company_id)