#!/usr/bin/env python
# -*- coding: utf-8 -*-
from Crypto.Cipher import AES
import base64
import hashlib
from mon4api.utils.crypt_util import pwd2

PADDING = '\0'
# PADDING = ' '
key = 'x' * 16
iv = 'x' * 16


def encrypt(data):
    generator = AES.new(key, AES.MODE_CBC, iv)
    data = _padding(data)
    crypt = generator.encrypt(data)
    return base64.b64encode(crypt)


def decrypt(data):
    generator = AES.new(key, AES.MODE_CBC, iv)
    data = base64.b64decode(data)
    recovery = generator.decrypt(data)
    return recovery.rstrip(PADDING)


def _padding(data):
    s = data + (16 - len(data) % 16) * PADDING
    return s


def md5(content):
    m = hashlib.md5(content)
    return m.hexdigest().upper()


def password(content):
    if not content: return content
    return encrypt(md5(content))


if __name__ == "__main__":
    s = encrypt("Test String")
    print s
    print decrypt(s)
    print password('2366')
    print pwd2('2366')

