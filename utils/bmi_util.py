# coding=utf-8
import format_util as formatUtil
import qingbii4common.util.string_util as stringUtil


def bmi(weight, height):
    if not weight or not height:
        return {'bmi': 0, 'bmi_type': '偏瘦'}
    bmi = float(weight) / (float(height) * 0.01) ** 2
    bmi = float(formatUtil.format_float_zero(round(bmi, 1)))
    bmi_type = ''
    if bmi < 18.5:
        bmi_type = '偏瘦'
    elif 18.5 <= bmi <= 23.9:
        bmi_type = '正常'
    elif 24 <= bmi <= 27.9:
        bmi_type = '超重'
    elif bmi >= 28:
        bmi_type = '肥胖'
    return {'bmi': bmi, 'bmi_type': bmi_type}


def health_weight(height):
    if not height:
        return {'health_from': 0, 'health_to': 0}
    weight_from = 18.5 * (float(height) * 0.01) ** 2
    weight_to = 23.9 * (float(height) * 0.01) ** 2
    return {'health_from': formatUtil.format_float_zero(round(weight_from, 1)),
            'health_to': formatUtil.format_float_zero(round(weight_to, 1))}


